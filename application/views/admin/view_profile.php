

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <title>Bulk Sms</title>
        <link id="" href="<?php echo base_url(); ?>css/bootstrap-cerulean.css" rel="stylesheet">
            <style type="text/css">
                /* CSS Document */
                a, abbr, acronym, address, applet, area, b, base, basefont, bdo, big, blockquote, body, br, button, caption, center, cite, code, col, colgroup, dd, del, dir, div, dfn, dl, dt, em, fieldset, font, form, frame, frameset, h1, h2, h3, h4, h5, h6, hr, i, iframe, img, input, ins, isindex, kbd, label, legend, li, link, map, menu, noframes, noscript, object, ol, optgroup, option, p, param, pre, q, s, samp, select, small, span, strike, strong, style, sub, sup, table, tbody, td, textarea, tfoot, th, thead, tr, tt, u, ul, var {
                    margin:0;
                    padding:0;
                    border:0;
                    outline:0;
                    font-size:100%;
                    vertical-align: baseline;
                    background:none;
                }
                ol, ul {
                    list-style:none;
                }
                h1, h2, h3, h4, h5, h6, li {
                    line-height:100%;
                }


                input, textarea, select {
                    font:11px Arial, Helvetica, sans-serif;
                    vertical-align:middle;
                    padding:0;
                    margin:0;
                }
                form, fieldset {

                }

                /* general setting */
                htm,body {
                    height:auto;
                    background: #fff;
                    color:#000;
                    font-size:12px;
                    font-family:Arial, Helvetica, sans-serif;
                }
                a {
                    text-decoration:none;
                    margin-left:10px;
                    margin-top:10px;
                }

                #print_page{
                    margin: auto;
                    overflow: hidden;
                }

                
                @media print {
                    body * {
                        visibility: visible;
                    }
                    #section_to_print, #section_to_print * {
                        visibility:visible;
                    }
                    #section_to_print {
                        position:absolute;
                        left:0;
                        top:0;
                    }
                }
            </style>

            <style>
                #print_page{
                    width: 120px;
                    height: 40px;
                    background: url(<?php echo base_url(); ?>images/printButton.gif) no-repeat;
                    margin: 50px 0 0 300px;
                    color: #fff;
                    font-weight: bold;
                    overflow: hidden;

                }

                .voucher{
                    margin: auto;
                    overflow: hidden;
                    width: 830px;

                }
                .voucher h1{
                    text-align: center;
                    text-transform: uppercase;
                    font-size: 40px;
                    margin-top: 10px;
                    font-family: arial;
                    color: #000;
                    padding-bottom: 15px;
                }
                .voucher ul{
                    width: 830px;
                    text-align: center;
                    width: 100%;
                    margin: auto;  color: #000;
                    overflow: hidden;

                }

                .voucher ul li{display:inline ;  color: #000;}
                .invalid_voucher{
                    margin:15px auto;
                    overflow: hidden;
                    width: 830px;
                    background: #c4c3c3;
                    border-radius:0.4em 0.4em 0.4em 0.4em; -moz-border-radius:0.4em 0.4em 0.4em 0.4em; -webkit-border-radius: 0.4em 0.4em 0.4em 0.4em; -khtml-border-radius: 0.4em 0.4em 0.4em 0.4em;
                    height: 35px;

                }
                .invalid_voucher h2{
                    text-align: center;
                    font-weight: normal;
                    font-size: 15px!important;
                    text-transform: uppercase;
                    line-height: 33px;
                    border-radius:0.4em 0.4em 0.4em 0.4em; -moz-border-radius:0.4em 0.4em 0.4em 0.4em; -webkit-border-radius: 0.4em 0.4em 0.4em 0.4em; -khtml-border-radius: 0.4em 0.4em 0.4em 0.4em;

                }
                .asdss li{
                    background: #F3F3F3;
                    padding: 10px;
                    overflow: hidden;

                    font-size: 13px;
                    border-radius:0.4em 0.4em 0.4em 0.4em; -moz-border-radius:0.4em 0.4em 0.4em 0.4em; -webkit-border-radius: 0.4em 0.4em 0.4em 0.4em; -khtml-border-radius: 0.4em 0.4em 0.4em 0.4em;
                }

                #buttoms{}
            </style>

            <!--slider end-->
    </head>
    <body>


<!--<script>
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}</script>-->

        <div id="non-printable"></div>

<!-- <style type="text/css" media="print">
.printbutton {
 visibility: hidden;
 display: none;
}

</style>-->

        <div id="printable">

            <div id="printableArea">
                <div class="voucher">
                    <h1>রামগঞ্জ সরকারী কলেজ</h1>
                    <ul>
                        <li>Address: 144,Motijheel ,Dhaka</li>

                    </ul>
                    <ul>
                        <li>Phone : 10120122</li>
                        <li>Fax : monir@gmail.com</li>
                        <li>Email : monirjss@gmail.com</li>
                        <li>Web : Dynamicsoftbd.com</li>
                    </ul>
                    <div class="invalid_voucher">
                        <h2>Student Profile</h2></div>
                    
                    <table style=" background: #f3f3f3; width:900px;  padding: 30px; margin: 15px auto; border-radius:0.4em 0.4em 0.4em 0.4em; -moz-border-radius:0.4em 0.4em 0.4em 0.4em; -webkit-border-radius: 0.4em 0.4em 0.4em 0.4em; -khtml-border-radius: 0.4em 0.4em 0.4em 0.4em;">
                        
                        <tr style=" float: left;padding-top: 5px; padding-left: 10px; display:  inline;width:145px;height:160px;background: #a3def1; margin-top: 15px; border-radius:0.4em 0.4em 0.4em 0.4em; -moz-border-radius:0.4em 0.4em 0.4em 0.4em; -webkit-border-radius: 0.4em 0.4em 0.4em 0.4em; -khtml-border-radius: 0.4em 0.4em 0.4em 0.4em;">
                            <td ><img src="<?php echo base_url().$s_info->image;?>" width="130" height="150" /></td>
                        </tr>
                        
                        <tr style=" margin-left: 10px; float: left; display: inline; width:300px; padding-bottom: 6px; background: #e1f2bb; margin-top: 15px; border-radius:0.4em 0.4em 0.4em 0.4em; -moz-border-radius:0.4em 0.4em 0.4em 0.4em; -webkit-border-radius: 0.4em 0.4em 0.4em 0.4em; -khtml-border-radius: 0.4em 0.4em 0.4em 0.4em; ">
                            <td>
                                <ul class="asdss" style="text-align: left; padding: 10px; ">
                                    <li style="width: 260px;margin-top: 3px; float: left; font-weight: bold">Name : <?php echo $s_info->s_name;?></li>
                                    <li style="width: 260px;margin-top: 3px; float: left;font-weight: bold">Class : <?php echo $s_info->class_name;?> </li>
                                    <li style="width: 260px;margin-top: 3px; float: left;font-weight: bold">Roll : <?php echo $s_info->s_roll;?> </li>
                                    <li style="width: 260px;margin-top: 3px; float: left;font-weight: bold">Gender : <?php echo $s_info->s_sex;?> </li>
                                    <li style="width: 260px;margin-top: 3px; float: left;font-weight: bold">Religion : <?php echo $s_info->s_religion;?> </li>
                                    
                                    <li style="width: 260px;margin-top: 3px; float: left;font-weight: bold">Academic Year : <?php echo $s_info->s_year;?> </li>
                                    <li style="width: 260px;margin-top: 3px; float: left;font-weight: bold">Phone : +88<?php echo $s_info->s_contact;?> </li>
                                    

                                </ul>
                                <a class="btn btn-info"  href="<?php echo base_url(); ?>student/manage_student" >  
                        <i clsss=""></i>Go Back Previous Page</a>
                            </td>
                        </tr>
                        <tr>
                            
                        </tr>
                    </table>


                    
                </div>

            </div>

        </div>

    </body>
</html>
