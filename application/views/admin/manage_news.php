<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Add Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">

            <fieldset>
                <legend>Add News Information</legend>
                <a class="btn btn-success" href="<?php echo base_url(); ?>administrator/add_news">
                    <i class="icon-edit icon-white"></i>  
                    Add News                                    
                </a>  
            </fieldset>


        </div>
    </div><!--/span-->

</div><!--/row-->
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 20px">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>News Title</th>
                        <th> News Description</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    $i = 0;
                    foreach ($all_info as $v_info) {
                        $i+=1;
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i; ?></td>
                            <td class="center" >
                                <?php echo $v_info->news_title;?>
                            </td>

                            <td class="center">
                                <?php echo $v_info->news_short_description;?>
                            </td>
                            <td class="center">
                                <?php echo $v_info->news_date;?>
                            </td>


                            <td class="center">
                                <a class="btn btn-info" href="<?php echo base_url(); ?>administrator/edit_news/<?php echo $v_info->news_id; ?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>  
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator/delete_news/<?php echo $v_info->news_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                            
                    </tr>
                    <?php
                        }
                        ?>

                </tbody>
            </table>

            

        </div>
    </div>
</div>
