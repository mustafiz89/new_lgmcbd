<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Edit News Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>

            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/update_news" method="post">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Notice Title(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="news_title" maxlength="100" required value="<?php echo $edit_info->news_title;?>">
                            <input type="hidden" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="news_id"  value="<?php echo $edit_info->news_id;?>">

                        </div>
                    </div>
                    
                   <div class="control-group">
                        <label class="control-label" for="textarea2">Short Description(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="news_short_description" id="textarea2" required rows="3" style="width:600px; height:40px;" maxlength="250"><?php echo $edit_info->news_short_description;?></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Long Description(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="news_long_description" id="textarea2" required rows="3" style="width:600px; height:300px; text-align: justify;"><?php echo $edit_info->news_long_description;?></textarea>
                        </div>
                    </div>
                    
                    

                       

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
