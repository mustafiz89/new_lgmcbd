
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Add Teacher's</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>

            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/save_teacher" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Name(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="name" required>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Designation(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="title" required>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Department</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="subject" >

                        </div>
                    </div>
<!--                    <div class="control-group">
                        <label class="control-label" for="typeahead">Joining Date</label>
                        <div class="controls">
                            <input type="text" class="xlarge datepicker" id="typeahead"  data-provide="typeahead" data-items="4" name="join_date" required><span style="margin-left: 10px; color: green;">(e.g 12/12/2015)</span>
                                <input type="text" class="input-append datepicker" data-date-format="dd-mm-yyyy" name="join_date" value="">


                        </div>
                    </div>-->
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Contact(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="contact" required>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Email(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="email" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="email" required>

                        </div>
                    </div>
                    
<!--                    <div class="control-group">
                        <label class="control-label" for="typeahead">Blood Group</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="blood_group" required>

                        </div>
                    </div>-->

                    <div class="control-group">
                        <label class="control-label" for="fileInput">Image(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input class="input-file uniform_on" id="fileInput" name="image" type="file" accept="image/*" required="file"><label><span style="color: green;">(N.B:File size should not be more than 3 MB and 2000*1200 pixel)</span></label>
                        </div>
                    </div>      

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
