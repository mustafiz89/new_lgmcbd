
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>College History</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            
            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/update_collage_history" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Title(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="history_title" value="<?php echo $history_info->history_title; ?>">
                            <input type="hidden" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="history_id" value="<?php echo $history_info->history_id; ?>">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    
                    
                                        
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Description(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="history_description" id="textarea2" required rows="3" style="width:600px; height:400px;"><?php echo $history_info->history_description;?></textarea>
                        </div>
                    </div>

                          
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
