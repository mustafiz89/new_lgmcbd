<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Add Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">

            <fieldset>
                <legend>Add Photo in Photo Gallary</legend>
                <a class="btn btn-success" href="<?php echo base_url(); ?>administrator/add_photo_gallary">
                    <i class="icon-edit icon-white"></i>  
                    Add Photo                                      
                </a>  
            </fieldset>


        </div>
    </div><!--/span-->

</div><!--/row-->
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Photo Gallary</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 20px">
            <?php
             $msg=$this->session->userdata('message');
             if($msg)
             {
                 echo $msg;
                 $this->session->unset_userdata('message');
             }
            ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Photo</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    $i=0;
                    foreach ($all_photo as $v_photo) {
                        $i+=1;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td class="center">
                                <img src="<?php echo base_url() . $v_photo->image; ?>" width="150" height="70" >
                            </td>
                            
                            <td class="center">
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator/delete_photo_gallary/<?php echo $v_photo->image_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                        </tr>
                    <?php
                        }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
