<style>
    table{
        border-collapse: collapse;
        border-spacing: 0.5rem;
        width:100%;

    }
    th,td{
        border: 1px solid #bab1b4;
        text-align: center;
        font-size: 14px;
        padding: 5px;
    }
    a{
        color: #004d66;
    }
</style>
<div class="profile_blog">
    <h2 class="title_head" ><?php echo $title; ?></h2>

    <span style="font-size: 20px;">
        <?php 
        if(count($result_info)>0)
        {
        ?>
        <table>
            <tr>
                <th>No</th>
                <th>Information</th>
                <th>Published Date</th>
                <th>Download Result</th>
            </tr>
            <?php
            $i = 0;
            foreach ($result_info as $v_info) {
                $i+=1;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $v_info->result_description; ?></td>
                    <td> <?php echo date("F j, Y", strtotime($v_info->result_date)); ?></td>
                    <td><a href="<?php echo base_url() . $v_info->result_file; ?>">(Download)</a></td>
                </tr>
                <?php
            }
                 ?>
            </table>
            <?php
        }
        ?>
    </span>
</div><!--profile_blog-->   

