<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <title>lgmc</title>
     <link rel="shortcut icon" href="<?php echo base_url();?>images/schoollogo/1.jpg" />
    <link href="<?php echo base_url();?>css/style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.lightbox-0.5.css" media="screen" />
 
  <!--slider-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/nivo-slider.css" />
  <script type="text/javascript" src="<?php echo base_url();?>js/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>js/js/jquery.nivo.slider.pack.js"></script>
   
  <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
  </script> 
    <!--slider-->
    <!--for menu-->

<script type="text/javascript">
			function handleImgError(srcElement) 
			{
		         srcElement.src="images/no.jpg";
		         srcElement.onerror = "";
		         return true;
			}
</script>

    
 </head>

<body>
<div id="wrapper">
<div id="header">
           <div id="logo"><a href=""><img  src="<?php echo base_url();?>images/schoollogo/1.jpg" height="100" width="110" alt="logo"/></a></div>
          <!--  <div class="social_link">
                <ul>
                    <li>Find us on&nbsp;&nbsp;</li>
                    <li><a href="#"><img  src="images/ff.png" alt="logo"/></a></li>
                    <li><a href="#"><img  src="images/ff.png" alt="logo"/></a></li>
                
                </ul>
            </div>-->
           <div style="margin-top: 40px;" class="school_name"><h1>লক্ষ্মীপুর সরকারী মহিলা কলেজ</h1></div>
           <!--<div class="gov_logo"><img  src="images/govlogo.png" alt="logo"/></div>-->
            <div id="navigation" style="width: 100%;margin-top: 122px;" >
            <div class="nav_blog">
            
               <div id='cssmenu'>
                <ul>
                        <li><a href="<?php echo base_url();?>welcome/index"><span>Home</span></a></li>
                       
                        <li class="has-sub "><a href="#"><span>About Us</span></a>
                            
                            <ul>
                            <li class="has-sub "><a href="<?php echo base_url();?>welcome/citizen_charter"><span>Citizen Charter</span></a>
                                   
                                </li>
                            <li class="has-sub "><a href="<?php echo base_url();?>welcome/principal_message"><span>Principal Message </span></a>
                                   
                                </li>
                            <li class="has-sub "><a href="<?php echo base_url();?>welcome/teachers_profile"><span>Teacher List</span></a>
                                 
                                </li>
                                
                                <li class="has-sub "><a href="<?php echo base_url();?>welcome/staff_list"><span>Staff List</span></a>
                                 
                                </li>
                              
                              
                               
                            </ul>
                            
                        </li>
                         <li class="has-su"><a href="<?php echo base_url();?>welcome/student_profile"><span>Student Profile</span></a>
                            
                            
                         </li> 
                         <li class="has-sub "><a href=""><span>Admission</span></a>
                           <ul>
                                <li class="has-sub "><a href="<?php echo base_url();?>welcome/admission_info"><span>Admission Info</span></a>
                                   
                                </li>
                               <li class="has-sub "><a href="<?php echo base_url();?>welcome/admission_form"><span>Admission Form</span></a>
                                   
                                </li>
                            </ul>
                     </li> 
                       <li class="has-sub "><a href=""><span>Exam</span></a>
                           <ul>
                                <li class="has-sub "><a href="<?php echo base_url();?>welcome/result"><span>Result</span></a>
                                    
                                </li>
                             
                                  <li class="has-sub "><a href="<?php echo base_url();?>welcome/exam_routine"><span>Exam  Routine</span></a>
                                   
                                </li>
                                 <li class="has-sub "><a href="<?php echo base_url();?>welcome/exam_seat"><span>Exam Seat info</span></a>
                                    
                                </li>
                            </ul>
                            
                        </li> 
                        
                    
                    
                     <li class="has-sub "><a href=""><span>Other's Info</span></a>
                            
                            <ul>
                                <li class="has-su"><a href="<?php echo base_url();?>welcome/notice"><span>Notice Board</span></a>
                            
                                </li> 
                                <li class="has-sub "><a href="<?php echo base_url();?>welcome/alternate_teacher"><span>Alternate Teacher info</span></a>
                                   
                                </li>
                                <li class="has-sub "><a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/photo_gallery" >Photo Gallery</a>                              

                                    
                                </li>
                                  <li class="has-sub "><a href="<?php echo base_url();?>welcome/carrier"><span>Post/Carrier</span></a>
                                   
                                </li>
                            </ul>      
                    </li>
                     <li class="has-sub "><a href=""><span>Download</span></a>
                            <ul>
                                <li class="has-sub "><a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/syllabus">Syllabus </a>
                                </li>
                                <li class="has-sub "><a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/book_list">Library</a>

                                </li>
                                <li class="has-sub "><a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/class_routine">Class Routine</a>

                                </li>
                                <li class="has-sub "><a href="<?php echo base_url();?>welcome/academic_calender"><span>Academic Calender</span></a>
                                   
                                </li>
                                
                            </ul>
                            
                     </li>
                        
                        
                         <li class="has-sub "><a href="<?php echo base_url();?>welcome/contact_us"><span>Contact Us</span></a>
                          
                            
                        </li>
                       
                      
                </ul>
           </div>
         </div>
            </div><!--navigation-->
       </div>
       <div style="width: 1010px; margin: auto; overflow: hidden; background:#fff;">
        <div class="notice_blog" style="   margin-bottom: 11px;margin-top: 58px;" >
            
            <marquee>
                <?php
                foreach($notice_info as $v_info)
                {
                ?>
                <a href="<?php echo base_url();?>welcome/notice_details/<?php echo $v_info->notice_id;?>"><span>**&nbsp; <?php echo $v_info->notice_title;?>&nbsp;** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></a>
                
                &nbsp;
               <?php 
                }
               ?>
            </marquee>
        </div>
</div>
        <script>
	  $("marquee").hover(function () { 
    this.stop();
}, function () {
    this.start();
});
	  </script>

<div id="container">
<!--<div id="sidebar">

          <div class="sidebarmenu">
                                                           
                            <a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/syllabus">Syllabus </a>
                            
                            <a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/book_list">Book List</a>
                              
                           
                               
                            <a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/class_routine">Class Routine</a>
                               
                            
                            <a class="menuitem submenuheader" href="<?php echo base_url();?>welcome/photo_gallery" >Photo Gallery</a>                              
                         
                           
                        </div>
						
          
</div>-->
<div id="content">

    
    <?php echo $mid_content;?>

</div>
</div>
<div id="footer">
       <div>
           
           <span>Copyright &copy; লক্ষ্মীপুর সরকারী মহিলা কলেজ - লক্ষ্মীপুর</span>
       
        </div>
    <div style="height: 10px; font-size: 10px;"><a href="http://www.dynamicsoftwareltd.com/" target="_blank">
       <h1 style="width: 230px;margin-left:262px; margin-bottom:-33px !important; ">Develop By-</h1> 
       <span><img src="<?php echo base_url();?>images/pow.png" height="40" width="300"/></a></span> 
    </div>
    </div>
</div>
</body>
</html>