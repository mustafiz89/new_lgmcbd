<style>
    .profile_blog span h3, h6{
        padding-left:5px;
    }
    .profile_blog span p{
        color:#3f393a;
        padding:5px;
        
    }
</style>
<div class="profile_blog">
    <h2 class="title_head" ><?php echo $title; ?></h2>
    <span style="font-size: 20px;">
        <h3><?php echo $all_info->notice_title;?></h3>
        <h6><?php echo date("F j, Y", strtotime($all_info->notice_date));?></h6>
        <hr>
        <p><?php echo $all_info->notice_long_description; ?></p>
    </span>
</div><!--profile_blog-->   
