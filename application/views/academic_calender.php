<style>
    table{
       border-collapse: collapse;
        border-spacing: 0.5rem;
        width:100%;
       
    }
    th,td{
       border: 1px solid #bab1b4;
        text-align: center;
        font-size: 14px;
        padding: 5px;
    }
    a{
        color: #004d66;
    }
</style>
 <div class="profile_blog">
             <h2 class="title_head" ><?php echo $title;?></h2>
             
            <span style="font-size: 20px;">
                <?php 
        if(count($calender_info)>0)
        {
        ?>
                
                <table>
                    <tr>
                        <th>No</th>
                        <th>Information</th>
                        <th>Date</th>
                        <th>Download </th>
                    </tr>
                    <?php 
                $i=0;
                foreach($calender_info as $v_info)
                {
                    $i+=1;
                ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $v_info->calender_description?></td>
                        <td><?php echo date("F j, Y", strtotime($v_info->calender_date));?></td>
                        <td><a href="<?php echo base_url().$v_info->calender_image;?>">(Download)</a></td>
                    </tr>
                    <?php 
                }
                ?>
                </table>
                <?php 
                }
                ?>
            </span>
        </div><!--profile_blog-->   
        
