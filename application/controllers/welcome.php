<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $data = array();
        $data['title'] = 'Home';
//        $data['all_syllabus']=$this->welcome_model->select_all_syllabus();
//        $data['class_routine']=$this->welcome_model->select_all_class_routine();
//        $data['book_list']=$this->welcome_model->select_all_book_list();
//        $data['all_slider'] = $this->welcome_model->select_all_slider();
//        $data['all_info'] = $this->welcome_model->select_news_for_home();
//        $data['principal_message'] = $this->welcome_model->select_principal_message_by_id(1);
//        $data['right_side_notice'] = $this->welcome_model->select_notice_for_right_side_bar();
//        $data['right_side_bar'] = $this->load->view('right_side_bar', $data, true);
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['welcome_info'] = $this->welcome_model->select_welcome_message(1);
        $data['mid_content'] = $this->load->view('home_content', $data, true);
        $this->load->view('master', $data);
    }
    
    public function welcome_message_details()
    {
        $data=array();
        $data['title']="Welcome Message";
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['welcome_info'] = $this->welcome_model->select_welcome_message(1);
        $data['mid_content'] = $this->load->view('welcome_message_details', $data, true);
        $this->load->view('master', $data);
    }
    public function principal_message() {
        $data = array();
        $data['title'] = 'Principal Message';
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['principal_message'] = $this->welcome_model->select_principal_message_by_id(1);
        $data['mid_content'] = $this->load->view('principal_message', $data, true);
        $this->load->view('master', $data);
    }
    
    public function teachers_profile() {
        $data = array();
        $data['title'] = "Teacher's Profile"; 
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['all_teacher_info'] = $this->welcome_model->select_all_teacher_info();
        //$data['right_side_bar']=$this->load->view('right_side_bar',$data,true);
        $data['mid_content'] = $this->load->view('teachers_profile', $data, true);
        $this->load->view('master', $data);
    }

    public function citizen_charter()
    {
        $data=array();
        $data['title']="Citizen's Charter";
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['all_info']=$this->welcome_model->select_charter_info_by_id(1);
        $data['mid_content'] = $this->load->view('citizen_charter', $data, true);
        $this->load->view('master', $data);
    }
    
    public function staff_list() {
        $data = array();
        $data['title'] = "Our Staff's";
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['all_staff_info'] = $this->welcome_model->select_all_staff_info();
        //$data['right_side_bar']=$this->load->view('right_side_bar',$data,true);
        $data['mid_content'] = $this->load->view('staff_list', $data, true);
        $this->load->view('master', $data);
    }
    
     

//    public function student_profile() {
//        $data = array();
//        $data['all_class']=$this->welcome_model->select_all_class();
//        $data['title'] = "Student Profile";
//        $data['right_side_notice'] = $this->welcome_model->select_notice_for_right_side_bar();
//        $data['right_side_bar']=$this->load->view('right_side_bar',$data,true);
//        $data['mid_content'] = $this->load->view('student_profile', $data, true);
//        $this->load->view('master', $data);
//    }
    public function student_profile()
    {
        $data=array();
        $data['title']='Student Profile';
//        $class_id=$this->input->post('class_id',true);
//         
//        
//        
//        $s_roll=$this->input->post('s_roll',true);
//        $data['all_class']=$this->welcome_model->select_all_class();
        
        
//        if ($class_id && $s_roll) {
//
//            $cdata = array();
//            $cdata['title'] = 'Student Profile';
//            $cdata['s_info'] = $this->welcome_model->select_student_by_roll($class_id, $s_roll);
//            if ($cdata['s_info']) {
//                $data['mid_content'] = $this->load->view('student_profile', $cdata, true);
//            } else {
//                $sdata = array();
//                $sdata['exception'] = 'No Student Data Found';
//                $this->session->set_userdata($sdata);
//            }
//        }
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['all_student_info']=$this->welcome_model->select_all_student();
        $data['mid_content'] = $this->load->view('student_profile', $data, true);
        $this->load->view('master', $data);
    }
    
    public function result(){
        $data = array();
        $data['title'] = "Result";
        $data['result_info']=$this->welcome_model->select_all_result();
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('result', $data, true);
        $this->load->view('master', $data);
    }
    public function class_routine()
    {
        $data = array();
        $data['title'] = "Class Routine";
        $data['routine_info']=$this->welcome_model->select_all_class_routine();
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('class_routine', $data, true);
        $this->load->view('master', $data);
    }
    public function exam_routine(){
        $data = array();
        $data['title'] = "Exam Routine";
        $data['routine_info']=$this->welcome_model->select_all_exam_routine();
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('exam_routine', $data, true);
        $this->load->view('master', $data);
    }
    public function syllabus(){
        $data = array();
        $data['title'] = "Syllabus";
        $data['syllabus_info']=$this->welcome_model->select_all_syllabus();
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('syllabus', $data, true);
        $this->load->view('master', $data);
    }
    
    public function book_list(){
        $data = array();
        $data['title'] = "Library";
        $data['list_info']=$this->welcome_model->select_all_book_list();
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('book_list', $data, true);
        $this->load->view('master', $data);
    }
    
    public function exam_seat()
    {
        $data = array();
        $data['title'] = "Exam Seat Info";
        $data['seat_info']=$this->welcome_model->select_all_exam_seat();
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('exam_seat', $data, true);
        $this->load->view('master', $data);
    }
    public function photo_gallery() {
        $data = array();
        $data['title'] = "Photo Gallary";
        $data['all_photo'] = $this->welcome_model->select_all_photo();
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('photo_gallery', $data, true);
        $this->load->view('master', $data);
    }
    
    public function notice() {
        $data = array();
        $data['title'] = "Notice";
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['all_info'] = $this->welcome_model->select_all_notice();
       
        $data['mid_content'] = $this->load->view('notice', $data, true);
        $this->load->view('master', $data);
    }

    public function notice_details($notice_id) {
        $data = array();
        $data['title'] = "Notice Details";
        $data['all_info'] = $this->welcome_model->select_notice_by_id($notice_id);
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('notice_details', $data, true);
        $this->load->view('master', $data);
    }

    public function news_events() {
        $data = array();
        $data['title'] = "News & Events";
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'welcome/news_events';

        $config['total_rows'] = $this->db->count_all('tbl_news');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        $this->pagination->initialize($config);
        $data['all_info'] = $this->welcome_model->select_all_news($config['per_page'], $this->uri->segment(3));
       
        $data['mid_content'] = $this->load->view('news_events', $data, true);
        $this->load->view('master', $data);
    }

    public function news_events_details($news_id) {
        $data = array();
        $data['title'] = "News & Events";

       
        $data['all_info'] = $this->welcome_model->select_news_by_id($news_id);
        $data['mid_content'] = $this->load->view('news_events_details', $data, true);
        $this->load->view('master', $data);
    } 
    public function alternate_teacher()
    {
        $data = array();
        $data['title'] = "Alternate Teacher Info";
        $data['teacher_info']=$this->welcome_model->select_alternate_teacher_info();
            $data['notice_info']=$this->welcome_model->select_all_notice(); 
        $data['mid_content'] = $this->load->view('alternate_teacher', $data, true);
        $this->load->view('master', $data);
    }
    public function carrier()
    {
        $data=array();
        $data['title']="Carrier Information";
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['carrier_info']=$this->welcome_model->select_carrier_info();
        $data['mid_content']=$this->load->view('carrier',$data,true);
        $this->load->view('master',$data);
    }
    public function academic_calender()
    {
        $data = array();
        $data['title'] = "Academic Calender";
        $data['calender_info']=$this->welcome_model->select_calender_info();
            $data['notice_info']=$this->welcome_model->select_all_notice(); 
        $data['mid_content'] = $this->load->view('academic_calender', $data, true);
        $this->load->view('master', $data);
    }
    
    public function admission_info()
    {
        $data=array();
        $data['title']='Admission Information';
        $data['all_admission_info']=$this->welcome_model->select_all_admission_info();
             $data['notice_info']=$this->welcome_model->select_all_notice();   
        $data['mid_content'] = $this->load->view('admission_info', $data, true);
        $this->load->view('master', $data);
    }
    
    public function admission_form()
    {
        $data=array();
        $data['title']='Admission Information';
        $data['all_admission_info']=$this->welcome_model->select_all_admission_form_info();
             $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('admission_info', $data, true);
        $this->load->view('master', $data); 
    }

    public function objection_corner()
    {
        $data = array();
        $data['title'] = "Objection Corner";
         $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('objection_corner', $data, true);
        $this->load->view('master', $data);
    }
    public function contact_us() {
        $data = array();
        $data['title'] = "Contact Us";
        $data['notice_info']=$this->welcome_model->select_all_notice();
        $data['mid_content'] = $this->load->view('contact_us', $data, true);
        $this->load->view('master', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */